import qualified Graphics.UI.GLFW as GLFW
import System.Exit (exitWith,ExitCode(..))
import Control.Monad (forever)

main :: IO ()
main = do
    Just version <- GLFW.getVersionString -- GLFWのバージョンデータをString型で取得する
    GLFW.setErrorCallback (Just errorCallback)
    putStrLn version
    glfwInitialized <- GLFW.init -- GLFWを初期化する
    putStrLn $ "status:" ++ (show glfwInitialized)
    
    Just win <- GLFW.createWindow width height title monitor window

    GLFW.defaultWindowHints -- ウィンドウに付随するデフォルト値をリセットする
    GLFW.makeContextCurrent (Just win)
    GLFW.setKeyCallback win (Just keyCallback)
    GLFW.setWindowCloseCallback win (Just shutdown)

    Just monitors <- GLFW.getMonitors
    putStrLn $ "monitors:" ++ (show monitors)

    monitorSizes <- mapM GLFW.getMonitorPhysicalSize monitors
    putStrLn $ "monitor sizes:" ++ (show monitorSizes)

    monitorNames <- map fromJust <$> mapM GLFW.getMonitorName monitors

    putStrLn $ "monitor names:" ++ (show monitorNames)


    forever $ do
        GLFW.pollEvents
        GLFW.swapBuffers win

    where
        width   = 400
        height  = 400
        title   = "Hello World"
        monitor = Nothing
        window  = Nothing

fromJust :: Maybe a -> a
fromJust (Just x) = x

showMaybe :: Show a => Maybe a -> String
showMaybe (Just x) = show x
showMaybe n        = show n

keyCallback :: GLFW.KeyCallback  -- Window -> Key -> Int -> KeyState -> ModifierKeys -> IO () -- 最終的な出力はIO ()
keyCallback win GLFW.Key'Q _ GLFW.KeyState'Pressed (GLFW.ModifierKeys True False False False) = shutdown win
keyCallback _   _          _ _                     _                                          = return ()

errorCallback :: GLFW.ErrorCallback
errorCallback error text = print $ (show error) ++ ":" ++ text

shutdown :: GLFW.WindowCloseCallback -- Window -> IO ()
shutdown win = do
    GLFW.destroyWindow win
    GLFW.terminate
    _ <- exitWith ExitSuccess
    return ()